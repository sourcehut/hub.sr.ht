This repository contains the code for the sr.ht project hub. For instructions on
deploying or contributing to this project, visit the manual here:

https://man.sr.ht/hub.sr.ht/installation.md
